# MiniGrid

## Description

MiniGrid is lightweight distribution of NanoTrix speech recognition platform.
Installation is initially performed on the master node. Slave nodes can be added afterwards.

## Configuration (master node)

1. Edit `.env`

    * set `PUBLIC_IP` and `PRIVATE_IP` a to your public and private interface IPv4 address
    * set `AUDIENCE` to publicly accessible domain/ip (e.g. http://mydomain.com or http://$MACHINE_IP), use `http://` scheme for insecure installation and `https://` for secure
    * set `CLUSTER_ID` and `V2T_TOKEN`  
    * *(recommended)* change `ADMIN_ID` and `ADMIN_SECRET` login credentials

1. Configure [Traefik](https://docs.traefik.io/v1.7/) by editing `traefik.toml` 
    * By default Traefik is configured with HTTP->HTTPS redirection with self signed certificate
    * Other options (see `traefik.toml` for instructions)
        * Automated Certificate Management Environment (ACME, e.g. traefik)
        * HTTPS with own certificates

1. *(optional, advanced)* Configure `gateway.json`

   * `localCache.ttlSeconds` - for how long to keep used tasks ready for next request
   * `proxy.allocationTimeoutSeconds` - how long to wait for free resources before giving up 
   * `proxy.inactivityTimeoutSeconds` - how long to wait when no data is sent by client before failing request

. 
1. *(mandatory for `online` licenses)* Enable automatic license management by setting `LICENSE_UPDATE_INTERVAL` to e.g. `24h`   
    

## Installation

1. Install prerequisites    
    * Ubuntu 18.04
        ```
        sudo bash prereqs_ubuntu.sh
        ```
    * CentOS 7
         ```
         sudo bash prereqs_centos.sh
         ```

1. Prefetch and backup docker images 
   Few GBs of data will be downloaded during this process (based on your license) - it may take some time.   
    ```
    sudo bash make-snapshot.sh 
    ```    
   This script will generate file called `snapshot.tar` that will contain all required data for recovery.    
   It is highly recommended to **backup `snapshot.tar` file onto reliable external storage** as it is used during slave installation and recovery in case of disk/filesystem failure.  

1. (skip if not using Traefik with ACME) Create empty `acme.json` with `0600` permissions
    ```
    echo '{}' | sudo tee acme.json
    chmod 0600 acme.json
    ```     
    
1. *(mandatory for online licenses)* Modify docker daemon to open port 2375 on **private** interface (used for docker image prefetch by auto-updater)
    ```
    sudo vi /lib/systemd/system/docker.service    
    
      ExecStart=/usr/bin/dockerd -H fd:// -H tcp://$PRIVATE_IP:2375
        
    sudo systemctl daemon-reload
    sudo systemctl restart docker
   ```
1. Install
    ```
    sudo docker-compose up -d
    ```
    
### Post installation checks

1. Check all components are up (some services may be in `Restarting` state for up a minute)
    ```
    sudo docker-compose ps

                    Name                                Command               State                                  Ports
    ------------------------------------------------------------------------------------------------------------------------------------------------------
    minigrid-installation_dashboard_1         ./nanogrid dashboard --zk  ...   Up
    minigrid-installation_gateway_1           ./nanogrid gateway file:/o ...   Up
    minigrid-installation_license-credit_1    ./nanogrid license credit  ...   Up
    minigrid-installation_license-local_1     ./nanogrid license cluster ...   Up
    minigrid-installation_license-updater_1   ./nanogrid license updater ...   Up
    minigrid-installation_mesos-master_1      mesos-master --registry=in ...   Up       X.X.X.X:5050->5050/tcp
    minigrid-installation_mesos-slave_1       mesos-slave                      Up       X.X.X.X:5051->5051/tcp
    minigrid-installation_store_1             ./nanogrid task store --zk ...   Up
    minigrid-installation_traefik_1           /traefik                         Up       X.X.X.X:443->443/tcp, X.X.X.X:7070->7070/tcp, X.X.X.X:80->80/tcp
    minigrid-installation_ui_1                /opt/run.sh nginx -g daemo ...   Up       80/tcp
    minigrid-installation_user-login_1        ./nanogrid auth user-login ...   Up
    minigrid-installation_user-management_1   ./nanogrid auth user-manag ...   Up
    minigrid-installation_zk_1                bash -ex /opt/exhibitor/wr ...   Up       X.X.X.X:2181->2181/tcp, 2888/tcp, 3888/tcp, X.X.X.X:8181->8181/tcp
    ```

1. Open $AUDIENCE in your browser 
    1. Login -> use admin credentials configured in `.env`
    1. License tab -> activate your cluster
    1. Cluster tab -> there should be at least one node
    1. Credit tab -> check if purchased credit is as expected

### Run test task

1. Open $AUDIENCE in your browser -> Transcribe tab

## How to

### Upgrade to new version

1. Backup your users and roles
1. Remove current installation
    ```
    docker-compose down
    ```
1. Backup current installation folder
    ```
    mv /path/to/current/minigrid-installation /path/to/current/minigrid-installation-backup
    ```
1. Clone upgrade version repo to `/path/to/current/minigrid-installation` (path must match)
    ```
    git clone -b ${upgrade_version} https://gitlab.com/nanotrix/minigrid-installation /path/to/current/minigrid-installation
    ```
1. Update `.env` to match your previous installation
1. Update `snapshot.tar`
    ```
    sudo bash make-snapshot.sh 
    ```
1. Install new version
    ```
    sudo bash prereqs.sh
    sudo docker-compose up -d
    ```

### Add slaves 

1. Clone this repository (the same version as on the master)
1. Install prereqs   
1. *(mandatory)* Edit `.env` (only following values need to be set - ignore other mandatory settings)
    * set `CLUSTER_ID`
    * set `PUBLIC_IP` and `PRIVATE_IP` 
    * set `MASTER_IP` to match your master node `PRIVATE_IP`

1. Import `snapshot.tar` created during master installation
    ```
    docker load --input snapshot.tar    
    ```
1. Install agent
    ```
    docker-compose -f docker-compose-add-slave.yml up -d
    ```
1. Check new agent is running
    ```
    docker-compose ps
    ```    
1. Check that agent successfully registered to the master (visit http://$MASTER_IP:5050 - agents tab)    

### Restart
```
docker-compose restart
```
## Api library

[NTX-JS](https://gitlab.com/nanotrix/ntx-js)
