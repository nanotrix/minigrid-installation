#!/bin/sh

DOCKER_VERSION=19.03.5
DOCKER_COMPOSE_VERSION=1.25.3

# docker
yum install -y curl yum-utils device-mapper-persistent-data lvm2
yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo

yum install -y docker-ce-${DOCKER_VERSION} docker-ce-cli-${DOCKER_VERSION}

systemctl enable docker.service
systemctl start docker.service

# docker compose
sudo curl -L "https://github.com/docker/compose/releases/download/${DOCKER_COMPOSE_VERSION}/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose


