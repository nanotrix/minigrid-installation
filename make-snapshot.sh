#!/usr/bin/env bash

source .env

# list compose images
for img in $(docker-compose config | awk '{if ($1 == "image:") print $2;}'); do
  COMPOSE_IMAGES="$COMPOSE_IMAGES $img"
done

# list engine images
HTTP_RESPONSE=$(curl --silent --header 'Content-Type:application/json' -X POST --data "{\"clusterId\":\"$CLUSTER_ID\"}" --write-out "HTTPSTATUS:%{http_code}" ${LICENSE}/preview/images)
HTTP_BODY=$(echo ${HTTP_RESPONSE} | sed -e 's/HTTPSTATUS\:.*//g')
HTTP_STATUS=$(echo ${HTTP_RESPONSE} | tr -d '\n' | sed -e 's/.*HTTPSTATUS://')

if [ ! ${HTTP_STATUS} -eq 200 ]; then
  echo "Error [HTTP status: $HTTP_STATUS, Body: $HTTP_BODY]"
  exit 1
fi

ENGINES=$(echo ${HTTP_BODY} | tr '\n' ' ')
#join
ALL_IMAGES="$COMPOSE_IMAGES $ENGINES"
#trim whitespace prefix
ALL_IMAGES="${ALL_IMAGES## }"
#deduplicate
ALL_IMAGES=$(echo ${ALL_IMAGES} | tr ' ' '\n' | sort -u)

IMG_COUNT=$(echo "$ALL_IMAGES" | wc -l)

echo "Downloading $IMG_COUNT images"

for img in ${ALL_IMAGES}; do
  docker pull ${img}
  if [ "$?" -ne 0 ]; then
    echo "Failed to pull $img"
    exit $?
  fi
done

echo "Generating snapshot.tar ($IMG_COUNT images)"
docker save -o snapshot.tar $(echo ${ALL_IMAGES} | tr '\n' ' ')
exit $?
